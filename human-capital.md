# Human Capital Cheat Sheet

## Keputusan Strategis HC

### Compensation and Benefits
- **Deskripsi:** Memastikan kebijakan kompensasi yang adil dan insentif yang sesuai.
- **Data Terkait:** Gaji, tunjangan, tingkat kinerja karyawan.
- **Bentuk Data:** Grafik batang untuk menampilkan rata-rata gaji dan tabel untuk menampilkan tunjangan serta tingkat kinerja karyawan.
- **Komparasi Data:** Perbandingan antara gaji, tunjangan, dan tingkat kinerja karyawan dengan periode sebelumnya, serta dengan standar industri.

### Retention Strategies
- **Deskripsi:** Mengurangi tingkat pergantian karyawan dan meningkatkan retensi.
- **Data Terkait:** Tingkat pergantian karyawan, alasan karyawan meninggalkan perusahaan.
- **Bentuk Data:** Grafik garis atau batang untuk menampilkan tren pergantian karyawan dan tabel untuk menampilkan alasan keluar.
- **Komparasi Data:** Perbandingan antara tingkat pergantian dan alasan keluar dengan periode sebelumnya, serta dengan standar industri.

### Workforce Productivity
- **Deskripsi:** Memantau dan meningkatkan produktivitas dan efisiensi tenaga kerja.
- **Data Terkait:** Produktivitas departemen atau tim, rasio pendapatan per karyawan.
- **Bentuk Data:** Grafik garis atau area untuk menampilkan tren produktivitas dan tabel untuk menyajikan rasio pendapatan per karyawan.
- **Komparasi Data:** Perbandingan antara produktivitas dan rasio pendapatan per karyawan dengan periode sebelumnya, serta dengan standar industri.

### Succession Planning
- **Deskripsi:** Mempersiapkan kelangsungan kepemimpinan dengan mengidentifikasi dan mengembangkan calon pemimpin masa depan.
- **Data Terkait:** Kesiapan calon pemimpin internal dan eksternal.
- **Bentuk Data:** Grafik batang atau lingkaran untuk menampilkan tingkat kesiapan calon pemimpin.
- **Komparasi Data:** Perbandingan antara kesiapan calon pemimpin internal dan eksternal dengan periode sebelumnya.

### Employee Engagement
- **Deskripsi:** Memantau dan meningkatkan tingkat keterlibatan dan kepuasan karyawan untuk meningkatkan produktivitas dan retensi.
- **Data Terkait:** Hasil survei karyawan tentang keterlibatan, tingkat kehadiran, dan tingkat pergantian karyawan.
- **Bentuk Data:** Skor keterlibatan karyawan, persentase tingkat kepuasan, dan grafik tren.
- **Komparasi Data:** Perbandingan antara tingkat keterlibatan dan kepuasan saat ini dengan periode sebelumnya, serta dengan standar industri atau perusahaan pesaing.

### Talent Acquisition
- **Deskripsi:** Meningkatkan efisiensi dan efektivitas proses perekrutan untuk menarik bakat berkualitas.
- **Data Terkait:** Jumlah aplikasi pekerjaan, waktu perekrutan, sumber rekrutmen, dan kualitas karyawan yang direkrut.
- **Bentuk Data:** Grafik batang untuk jumlah aplikasi dan waktu perekrutan, serta tabel untuk menampilkan kualifikasi karyawan yang direkrut.
- **Komparasi Data:** Perbandingan antara biaya perekrutan, waktu perekrutan, dan kualitas karyawan dengan periode sebelumnya, serta dengan standar industri.

### Talent Development
- **Deskripsi:** Mengidentifikasi kebutuhan pelatihan dan pengembangan karyawan untuk meningkatkan keterampilan dan kompetensi.
- **Data Terkait:** Hasil evaluasi kinerja karyawan, partisipasi program pelatihan, dan pertumbuhan kinerja setelah pelatihan.
- **Bentuk Data:** Grafik garis untuk menampilkan pertumbuhan kinerja dan tabel untuk melihat partisipasi pelatihan.
- **Komparasi Data:** Perbandingan antara tingkat partisipasi dan kinerja setelah pelatihan dengan periode sebelumnya, serta dengan standar industri.

### Performance Management
- **Deskripsi:** Menilai dan mengelola kinerja karyawan untuk meningkatkan produktivitas.
- **Data Terkait:** Penilaian kinerja karyawan, pencapaian target kerja.
- **Bentuk Data:** Grafik batang atau lingkaran untuk menampilkan penilaian kinerja dan tabel untuk menyajikan pencapaian target.
- **Komparasi Data:** Perbandingan antara penilaian kinerja dan pencapaian target dengan periode sebelumnya, serta dengan standar industri.

### Diversity and Inclusion
- **Deskripsi:** Meningkatkan keberagaman dan inklusivitas di tempat kerja.
- **Data Terkait:** Data demografi karyawan, tingkat keterlibatan dan kepuasan berdasarkan latar belakang demografis.
- **Bentuk Data:** Grafik lingkaran atau batang untuk menampilkan representasi keberagaman karyawan dan tingkat keterlibatan berdasarkan demografi.
- **Komparasi Data:** Perbandingan antara representasi keberagaman dan tingkat keterlibatan berdasarkan demografi dengan periode sebelumnya.

### Organizational Culture
- **Deskripsi:** Mempertahankan budaya perusahaan yang mendukung nilai-nilai inti dan visi organisasi.
- **Data Terkait:** Hasil survei budaya perusahaan, tingkat keterlibatan karyawan.
- **Bentuk Data:** Grafik batang atau garis untuk menampilkan hasil survei budaya dan tabel untuk menampilkan tingkat keterlibatan karyawan.
- **Komparasi Data:** Perbandingan antara hasil survei budaya dan tingkat keterlibatan karyawan dengan periode sebelumnya.

## Performance Indicator

### Salary Cost per Department

Dalam mengelola sumber daya manusia (Human Capital), memantau biaya gaji menjadi hal penting bagi manajer SDM dan pembuat keputusan. Informasi ini memberikan gambaran tentang bagaimana sebagian besar biaya gaji berasal dari departemen mana dan bagaimana perkembangannya dari waktu ke waktu. Hal ini juga membantu dalam merencanakan strategi keuangan dan penganggaran untuk organisasi secara keseluruhan. Biaya yang termasuk dalam penghitungan metrik ini mencakup gaji pokok, pajak, tunjangan, dan lainnya.

#### Description

Dalam membaca informasi tentang biaya gaji, ada beberapa poin penting yang perlu diperhatikan:
1. **Departemen dengan Biaya Gaji Tertinggi:** Identifikasi departemen yang menyumbang sebagian besar biaya gaji. Fokus pada departemen ini dapat membantu mengoptimalkan penggunaan anggaran untuk tenaga kerja.
2. **Perkembangan Biaya Gaji dari Waktu ke Waktu:** Analisis tren perubahan biaya gaji dari bulan ke bulan atau tahun ke tahun. Apakah ada peningkatan atau penurunan biaya gaji dari waktu ke waktu? Jika ada, apa penyebabnya?
3. **Dampak Biaya Gaji terhadap Pendapatan:** Evaluasi bagaimana biaya gaji berdampak pada pendapatan keseluruhan perusahaan. Apakah ada korelasi antara biaya gaji yang tinggi dengan peningkatan pendapatan atau sebaliknya?
4. **Strategi Penganggaran dan Keuangan:** Informasi ini harus menjadi pertimbangan dalam perencanaan anggaran dan strategi keuangan perusahaan. Manajer SDM dan tim keuangan dapat bekerja sama untuk menentukan alokasi anggaran yang efisien untuk setiap departemen berdasarkan kinerja biaya gaji mereka.
5. **Pengukuran Efisiensi Tenaga Kerja:** Biaya gaji per karyawan dapat menjadi metrik untuk mengukur efisiensi tenaga kerja di berbagai departemen. Departemen dengan biaya gaji rendah per karyawan mungkin lebih efisien dalam penggunaan sumber daya manusia mereka.

#### How to Read
Dalam membaca data biaya gaji, ikuti langkah-langkah berikut:

1. **Identifikasi Departemen Utama:** Perhatikan departemen yang memiliki kontribusi terbesar dalam total biaya gaji perusahaan. Ini akan membantu mengetahui area mana yang memerlukan perhatian khusus dalam pengelolaan anggaran.
2. **Analisis Tren:** Periksa tren perubahan biaya gaji dari waktu ke waktu. Amati apakah ada pola peningkatan atau penurunan biaya dan cari penyebab potensialnya.
3. **Evaluasi Dampak pada Pendapatan:** Tinjau bagaimana biaya gaji mempengaruhi pendapatan keseluruhan perusahaan. Pertimbangkan apakah ada kesempatan untuk meningkatkan pendapatan dengan mengelola biaya gaji secara efisien.
4. **Manfaatkan Data untuk Pengambilan Keputusan:** Gunakan informasi ini untuk merencanakan strategi penganggaran dan keuangan. Tetapkan sasaran efisiensi tenaga kerja dan gunakan metrik biaya gaji per karyawan untuk menilai kinerja departemen.

### Headcount Development
Melacak perkembangan headcount memungkinkan manajer SDM dan pengambil keputusan untuk memahami bagaimana jumlah karyawan berubah dari waktu ke waktu, dan bagaimana hal itu mempengaruhi kinerja dan efisiensi perusahaan secara keseluruhan. Informasi ini dapat menjadi indikator kunci (KPI) yang relevan dalam mengelola dan mengoptimalkan tenaga kerja.

#### Description
Dalam memahami pengembangan headcount, perhatikan beberapa poin penting berikut:
1. **Perubahan Jumlah Karyawan:** Identifikasi tren dalam perubahan jumlah karyawan dari periode ke periode. Apakah headcount meningkat, menurun, atau tetap stabil? Jika ada perubahan, pelajari penyebabnya, seperti ekspansi bisnis, restrukturisasi, atau perubahan kebijakan perekrutan.
2. **Pengaruh Terhadap Kinerja Perusahaan:** Analisis dampak dari perkembangan headcount terhadap kinerja perusahaan. Pertimbangkan apakah perubahan jumlah karyawan berkontribusi pada peningkatan produktivitas dan pendapatan atau sebaliknya.
3. **Efisiensi Tenaga Kerja:** Tinjau hubungan antara pertumbuhan headcount dengan efisiensi tenaga kerja. Apakah pertumbuhan tersebut mendukung upaya untuk menutupi beban kerja dengan lebih efisien, ataukah ada potensi untuk mengoptimalkan jumlah karyawan?
4. **Tingkat Pemenuhan Kebutuhan Tenaga Kerja:** Evaluasi apakah pengembangan headcount memadai untuk memenuhi kebutuhan tenaga kerja yang diperlukan dalam perusahaan. Pastikan perusahaan memiliki jumlah karyawan yang tepat untuk mengatasi tuntutan operasional dan strategi pertumbuhannya.
5. **Faktor Eksternal dan Internal:** Pertimbangkan faktor eksternal, seperti perubahan pasar atau regulasi, yang dapat mempengaruhi keputusan pengembangan headcount. Selain itu, evaluasi faktor internal, seperti kebijakan pengelolaan sumber daya manusia, untuk menilai sejauh mana pengembangan headcount terkait dengan strategi organisasi.

#### How to Read
Dalam membaca informasi tentang pengembangan headcount, perhatikan langkah-langkah berikut:
1. **Analisis Perubahan Jumlah Karyawan:** Tinjau data tentang perubahan jumlah karyawan dari waktu ke waktu. Identifikasi pola atau tren yang signifikan.
2. **Korelasi dengan Kinerja Perusahaan:** Periksa apakah terdapat korelasi antara perkembangan headcount dan kinerja perusahaan. Amati apakah peningkatan headcount berdampak positif atau negatif pada kinerja perusahaan secara keseluruhan.
3. **Evaluasi Efisiensi Tenaga Kerja:** Hitung rasio efisiensi tenaga kerja untuk membandingkan pertumbuhan headcount dengan kinerja perusahaan. Hal ini akan membantu menilai apakah jumlah karyawan yang ada sudah cukup efisien atau perlu dioptimalkan.
4. **Perhatikan Penyebab Perubahan Headcount:** Pelajari penyebab perubahan jumlah karyawan, termasuk faktor eksternal dan kebijakan internal perusahaan. Hal ini akan membantu dalam merencanakan strategi pengembangan headcount ke depan.
5. **Perencanaan Pengembangan Headcount:** Berdasarkan analisis data, buat rencana pengembangan headcount yang sesuai dengan kebutuhan dan strategi perusahaan. Pastikan peningkatan headcount didukung oleh pertimbangan ekonomi dan operasional yang matang.

### Turnover Rate
Mengukur persentase karyawan yang meninggalkan perusahaan dalam suatu periode waktu tertentu. Tingkat pergantian yang tinggi dapat menandakan adanya masalah dalam retensi karyawan, yang pada gilirannya dapat berdampak negatif pada stabilitas tenaga kerja, biaya rekrutmen, dan produktivitas organisasi. Memahami dan mengelola Turnover Rate secara efektif membantu perusahaan meningkatkan kinerja sumber daya manusia dan menciptakan lingkungan kerja yang lebih menarik dan berkelanjutan.

#### Description
Turnover Rate dihitung dengan membagi jumlah karyawan yang keluar dari perusahaan dalam periode waktu tertentu dengan total jumlah karyawan pada awal periode tersebut, dan kemudian mengalikannya dengan 100 untuk mendapatkan persentasenya.

Contoh perhitungan Turnover Rate:

Jumlah karyawan yang keluar dalam setahun = 30
Total jumlah karyawan pada awal tahun = 500

Turnover Rate = (30 / 500) * 100 = 6%

Turnover Rate dapat diukur dalam periode waktu tertentu, seperti bulanan, kuartalan, atau tahunan. Dengan memantau dan mencatat Turnover Rate secara teratur, perusahaan dapat mengidentifikasi tren dan fluktuasi dalam tingkat pergantian karyawan.

#### How To Read
Untuk menginterpretasikan Turnover Rate dengan baik, perhatikan hal berikut:
1. **Tingkat Pergantian Normal:** Setiap industri atau jenis pekerjaan mungkin memiliki tingkat pergantian normal yang dapat diterima. Perusahaan harus memahami benchmark atau rata-rata tingkat pergantian untuk sektor mereka dan membandingkan Turnover Rate mereka dengan angka ini. Jika Turnover Rate berada di atas rata-rata, ini dapat menunjukkan adanya masalah yang perlu diatasi.
2. **Analisis Penyebab:** Jika Turnover Rate tinggi, lakukan analisis mendalam untuk mengidentifikasi penyebabnya. Beberapa penyebab umum pergantian karyawan adalah ketidakpuasan pekerjaan, kurangnya kesempatan pengembangan karir, masalah manajemen, atau kondisi kerja yang tidak sesuai. Dengan memahami penyebab pergantian, perusahaan dapat mengambil tindakan perbaikan yang lebih tepat dan efektif.
3. **Fokus pada Pengelolaan Talent:** Tingkat pergantian yang tinggi dapat menyebabkan kehilangan talenta berharga dan pengetahuan di dalam organisasi. Fokus pada pengelolaan talenta, pengembangan karir, dan program retensi dapat membantu mempertahankan karyawan berkinerja tinggi dan mengurangi tingkat pergantian.
4. **Efek pada Biaya dan Produktivitas:** Hitung biaya rekrutmen dan pelatihan ulang sebagai akibat dari tingkat pergantian yang tinggi. Tingkat pergantian yang tinggi dapat menyebabkan beban finansial yang signifikan dan mengurangi produktivitas karena perlu waktu untuk membiasakan diri dengan karyawan baru.
5.  **Perbaikan dan Tindakan Korektif:** Setelah mengidentifikasi penyebab pergantian, lakukan perbaikan dan tindakan korektif yang sesuai. Perusahaan dapat mengimplementasikan program pengembangan karyawan, memperbaiki manajemen kinerja, dan meningkatkan komunikasi untuk meningkatkan retensi karyawan.

### Position Level (Succession Planning)
Informasi ini membantu manajemen untuk memiliki wawasan yang lebih jelas tentang keseimbangan usia, pengalaman, dan keterampilan di berbagai level jabatan. Melalui data demografi ini, perusahaan dapat mengidentifikasi calon pengganti potensial, mengelola risiko kehilangan karyawan berpengalaman, serta merencanakan strategi suksesi yang efektif untuk mencapai kesuksesan jangka panjang.
#### Description
Data demografi berdasarkan tingkat jabatan mencakup informasi tentang distribusi usia, tingkat pengalaman, dan keterampilan karyawan di berbagai level organisasi. Ini termasuk data tentang jumlah karyawan pada setiap tingkat jabatan, usia rata-rata di masing-masing level, tingkat turnover pada tiap tingkat, dan kemampuan serta keterampilan yang dimiliki oleh karyawan pada level jabatan tertentu.

Data ini juga mencerminkan keterwakilan diversitas dalam organisasi, termasuk perbandingan latar belakang, gender, dan budaya di berbagai level jabatan. Dengan menggabungkan data demografi ini dengan informasi lain, manajemen dapat mengidentifikasi calon pengganti potensial di setiap tingkatan dan mengukur tingkat kesiapan organisasi dalam menghadapi pergantian pemimpin atau posisi kunci.

#### How To Read
1. **Analisis Distribusi Usia dan Pengalaman:** Perhatikan piramida usia organisasi dan identifikasi distribusi usia karyawan di berbagai tingkat jabatan. Evaluasi apakah ada kesenjangan usia atau kelebihan konsentrasi karyawan pada tingkat tertentu yang dapat mempengaruhi suksesi.
2. **Tingkat Keterwakilan Diversitas:** Tinjau keterwakilan latar belakang, gender, dan budaya pada setiap level jabatan. Identifikasi apakah suksesi mencerminkan keberagaman dan inklusivitas dalam organisasi.
3. **Tingkat Pengalaman di Setiap Tingkat Jabatan:** Perhatikan usia rata-rata dan pengalaman karyawan di berbagai level jabatan. Evaluasi apakah karyawan di posisi kunci memiliki pengalaman yang cukup untuk mengisi peran tersebut dengan sukses.
4. **Tingkat Pergantian Karyawan:** Analisis tingkat pergantian karyawan di setiap tingkat jabatan. Perhatikan apakah ada tingkat turnover yang tinggi pada posisi kunci atau apakah karyawan muda cenderung meninggalkan organisasi.
5. **Kesiapan Suksesi:** Evaluasi kesiapan suksesi dengan mempertimbangkan data demografi. Identifikasi calon pengganti potensial dan perencanaan pengembangan yang sesuai untuk mengisi peran kunci di masa depan.
6. **Perbandingan dengan Benchmark Industri:** Bandingkan data demografi organisasi dengan benchmark industri untuk menilai kinerja relatif dan mengidentifikasi peluang perbaikan.
7. **Perencanaan Strategis:** Gunakan data demografi untuk merencanakan strategi suksesi yang efektif dan berkelanjutan. Identifikasi area yang memerlukan perbaikan, seperti program pengembangan dan pelatihan, untuk mengoptimalkan pengisian posisi kunci.
